const express = require('express')
const puppeteer = require('puppeteer');
//const sharp = require('sharp');
const uuidv1 = require('uuid/v1');
const bodyParser = require('body-parser');
const fs = require('fs');
const sgMail = require('@sendgrid/mail');


var logger = require('logzio-nodejs').createLogger({
    token: 'lKBBHTDUJxGtBLsbyoGNjTCKLoDuhiMP',
    host: 'listener.logz.io',
    type: 'backend'     // OPTIONAL (If none is set, it will be 'nodejs')
});

var http = require('http');
var https = require('https');

try {
  var privateKey  = fs.readFileSync('/etc/letsencrypt/live/natthimlenposter.se/privkey.pem', 'utf8');
  var certificate = fs.readFileSync('/etc/letsencrypt/live/natthimlenposter.se/fullchain.pem', 'utf8');
  var credentials = {key: privateKey, cert: certificate};
} catch (e) {
  var privateKey  = fs.readFileSync('ssl/localhost.key', 'utf8');
  var certificate = fs.readFileSync('ssl/localhost.crt', 'utf8');
  var credentials = {key: privateKey, cert: certificate};

} finally {}

const app = express();
const http_port = 80;
const https_port = 443;

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies


(async () => {
  const browser = await puppeteer.launch({args: ['--no-sandbox', '--disable-setuid-sandbox'], ignoreHTTPSErrors: true, dumpio: false });

function is_dev_env() {
	return __filename.includes('git'); // if git in the path, this is dev env, otherwise send to log server
}

function app_loger(msg) {
	if (is_dev_env()) {
		console.log(msg);
	}
	else {
		logger.log(msg);
	}
}

app.post('/generate_order', async (req,res) => {
	var order_id = Date.now().toString().substring(2,11);
	app_loger('yay! an order submitted, generate_order begin'+ order_id);
	app_loger('full order id:' + order_id + ' details: ' + JSON.stringify(req.body));
	app_loger('payment token: ' +  req.body.payment_token);

	var output_filename = "order_"+order_id;
	app_loger('order_id is: '+ order_id);
	app_loger('order request:' + req.body.items + '  id:' + order_id)
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth() + 1; //January is 0!
	var yyyy = today.getFullYear();
	if (dd < 10) dd = '0' + dd;
	if (mm < 10) mm = '0' + mm;
	var order_date = dd + '-' + mm + '-' + yyyy;
	var order_confirmation_template;

  var order_raw_details_email="order_id: " + order_id;
  order_raw_details_email +=  "\norder_details: " + JSON.stringify(req.body);

	if (req.body.payment_token=="") {
		order_confirmation_template = fs.readFileSync('order_confirmation_template.html', 'utf8');
	}
	else {
		order_confirmation_template = fs.readFileSync('payment_confirmation_template.html', 'utf8');
	}

	var order_confirmation_item_template = fs.readFileSync('order_confirmation_item_template.html', 'utf8');
	var order_confirmation_items_content = "";
	app_loger('numer of items in order_id is: '+ req.body.items.length.toString() + ' id: ' + order_id );

	for (i = 0; i < req.body.items.length; i++) {
	    var local_item = JSON.parse(req.body.items[i]);
	    var order_item = order_confirmation_item_template;

	    var full_item_spec = local_item.item_name;
  		full_item_spec += "<br>Time: " + local_item.item_metadata.time_hours + ":" + local_item.item_metadata.time_minutes;
  		full_item_spec += "<br>Longitude: " + local_item.item_metadata.location_lng;
  		full_item_spec += "<br>Latitude: " + local_item.item_metadata.location_lat;
  		full_item_spec += "<br>Color: " + local_item.item_metadata.bg_color_name;
  		full_item_spec += "<br>Star glare: " + local_item.item_metadata.is_star_glare.toString().replace("false","no").replace("true","yes");
  		full_item_spec += "<br>Constellations: " + local_item.item_metadata.is_constellations.toString().replace("false","no").replace("true","yes");
  		full_item_spec += "<br>White background: " + local_item.item_metadata.is_white_bg.toString().replace("false","no").replace("true","yes");
  		full_item_spec += "<br>Grid lines: " + local_item.item_metadata.is_grid_lines.toString().replace("false","no").replace("true","yes");
      full_item_spec += "<br>Extra Text:<br>" + decodeURIComponent(local_item.item_metadata.text_extra);

	    order_item = order_item.replace(/{{item_title}}/g, full_item_spec);
	    order_item = order_item.replace(/{{item_amount}}/g, local_item.item_count) ;
	    order_item = order_item.replace(/{{item_price}}/g, (parseInt(local_item.item_price) * parseInt(local_item.item_count)).toString()+ " " + req.body.currency);
	    if (is_dev_env()) {
			order_item = order_item.replace(/{{item_img_link}}/g, 'http://localhost' + local_item.item_img.replace("_t.png",".png"));
	    }
	    else
	    {
	    	order_item = order_item.replace(/{{item_img_link}}/g, 'https://natthimlenposter.se' + local_item.item_img.replace("_t.png",".png"));
	    }

	    order_confirmation_items_content += order_item;
	  }


	order_confirmation_template = order_confirmation_template.replace("{{item_list}}",order_confirmation_items_content);
	order_confirmation_template = order_confirmation_template.replace(/{{order_id}}/g, order_id);
	order_confirmation_template = order_confirmation_template.replace(/{{order_date}}/g, order_date);
	order_confirmation_template = order_confirmation_template.replace(/{{shipping_address_name}}/g, req.body.first_name + ' ' + req.body.last_name);
	order_confirmation_template = order_confirmation_template.replace(/{{shipping_address_street}}/g, req.body.street);
	order_confirmation_template = order_confirmation_template.replace(/{{shipping_address_city_zip}}/g, req.body.city + ' ' + req.body.zip + ', ' + req.body.country);
  if (req.body.cart_cost_without_promo!=req.body.cart_cost)
  {
    app_loger('promo2020 applied');
    order_confirmation_template = order_confirmation_template.replace(/{{order_price_cart}}/g, "<del>" + req.body.cart_cost_without_promo+ " " + req.body.currency  + "</del> " + req.body.cart_cost+ " " + req.body.currency) ;
  }
  else {
    order_confirmation_template = order_confirmation_template.replace(/{{order_price_cart}}/g, req.body.cart_cost+ " " + req.body.currency) ;
  }
	order_confirmation_template = order_confirmation_template.replace(/{{order_price_shipping}}/g, req.body.shipping_cost+ " " + req.body.currency) ;
	order_confirmation_template = order_confirmation_template.replace(/{{order_price_total}}/g, req.body.total_cost+ " " + req.body.currency) ;


	fs.writeFileSync('generated_orders/' + output_filename+ '.html', order_confirmation_template);

	app_loger('generate_order generating pdf' + ' id: ' + order_id);
	var page = await browser.newPage();
	await page.goto('http://0.0.0.0:'+http_port+'/generated_orders/'+ output_filename+ '.html', {timeout: 5000, "waitUntil": "networkidle0"});
	await page.pdf({path: 'generated_orders/' + output_filename+ '.pdf', format: 'A4'});
 	app_loger('generate_order generating pdf complete' + ' id: ' + order_id);

	app_loger('generate_order - closing page: id:' + order_id);
	await page.close();
	var pages = await browser.pages();
	app_loger('number of open pages: ' + pages.length);




 	if (req.body.payment_token!="") {
 		app_loger('generating charge order_id: ' + order_id + '  payment token: ' +  req.body.payment_token);

		var stripe;
 		if (is_dev_env()){
			stripe = require("stripe")('sk_test_TbZBKJgk96npw0g1okKvX7aZ');
		}
		else {
			stripe = require("stripe")('sk_live_Z0o3fBCMqrxFxwyVuBEUz75q');
		}

		var charge = stripe.charges.create({
		  amount: parseInt(req.body.total_cost) * 100,
		  currency: 'sek',
		  source: req.body.payment_token,
		  receipt_email: req.body.email.trim(),
		});

	}

	sgMail.setApiKey("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");
	var msg1 = {
	  to: req.body.email.trim(),
	  from: 'Natthimlenposter <orders@natthimlenposter.se>',
	  subject: 'Your personal night sky poster - order confirmation',
	  html: order_confirmation_template,
	};
	sgMail.send(msg1);

	var msg2 = {
	  to: "princeiliya@gmail.com",
    from: 'Natthimlenposter <orders@natthimlenposter.se>',
	  subject: 'order confirmation, id:' + order_id + ' email: ' + req.body.email.trim() + ' phone: ' + req.body.phone.trim() + ' payment:' + req.body.payment_token,
	  html: order_confirmation_template,
	};
	sgMail.send(msg2);

  var msg3 = {
    to: "princeiliya@gmail.com",
    from: 'Natthimlenposter <orders@natthimlenposter.se>',
    subject: 'order confirmation, id:' + order_id + ' email: ' + req.body.email.trim() + ' phone: ' + req.body.phone.trim() + ' payment:' + req.body.payment_token,
    text: order_raw_details_email,
  };
  sgMail.send(msg3);

	app_loger('generate_order end' + ' id: ' + order_id);
	res.send(order_id);
});

app.post('/send_contactus', async (req,res) => {
	app_loger('send_contactus begin');

	var content_txt = "";
	content_txt += "contacts-first-name: " + req.body.fname + "\r\n";
	content_txt += "contacts-last-name: " + req.body.lname + "\r\n";
	content_txt += "contacts-email: " + req.body.email + "\r\n";
	content_txt += "contacts-phone: " + req.body.phone + "\r\n";
	content_txt += "contacts-message: " + req.body.message + "\n";


	sgMail.setApiKey("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");

	var msg1 = {
	  to: "info@natthimlenposter.se",
	  from: 'info@natthimlenposter.se',
	  subject: 'website contact form',
	  text: content_txt,
	};
	sgMail.send(msg1);
	app_loger('send_contactus end');
	res.send("MF000");
});


app.get('/generate_map', async (req,res) => {
	var rnd_file_name = uuidv1();
	app_loger('generate_map begin id:' + rnd_file_name);

	var loc = req.query.loc;
	var loc_lng = req.query.loc_lng;
	var loc_lat = req.query.loc_lat;
	var date = req.query.date;
	var text_extra = req.query.text_extra;
	var bg_color = req.query.bg_color;

	var is_glare = req.query.is_glare;
	var is_constellations = req.query.is_constellations;
	var is_white_bg = req.query.is_white_bg;
	var is_grid = req.query.is_grid;
  var size = req.query.size;


	var map_url_params="";
	map_url_params += "loc="+loc;
	map_url_params += "&loc_lng="+loc_lng;
	map_url_params += "&loc_lat="+loc_lat;
	map_url_params += "&text_extra="+encodeURIComponent(text_extra)+encodeURIComponent(" ");
	map_url_params += "&bg_color="+bg_color;
	map_url_params += "&is_glare="+is_glare;
	map_url_params += "&is_constellations="+is_constellations;
	map_url_params += "&is_grid="+is_grid;
	map_url_params += "&is_white_bg="+is_white_bg;
	map_url_params += "&date="+date;

  var y_hight=800;
  if (size=='20x30' || size == '40x60') {
    map_url_params += "&content_y=810";
    y_hight=810;
  }
  else if (size=='30x40') {
    map_url_params += "&content_y=720";
    y_hight=720;
  }
  else if (size=='50x70') {
    map_url_params += "&content_y=756";
    y_hight=756;
  }


	var page = await browser.newPage();
	await page.setViewport({ width: 540, height: y_hight});
	if (is_glare=='false') {
		app_loger('generate_map no glare query:' + map_url_params + " id:" + rnd_file_name);
		await page.goto('http://0.0.0.0:'+http_port+'/secret/iliya_s.html?'+map_url_params, {timeout: 5000, waitUntil: ['domcontentloaded']});
	}
	else {
		app_loger('generate_map with glare query:' + map_url_params+ " id:" + rnd_file_name);
		if (bg_color=="010203" || bg_color=="1a1f2a") {
			map_url_params = "glare_color=blue&"+map_url_params; // set star glare to blue to if black map (important to keep the date at the end)
		}
		else {
			map_url_params = "glare_color=white&"+map_url_params; // set star glare to white if not black map (important to keep the date at the end)
		}

		await page.goto('http://0.0.0.0:'+http_port+'/secret/iliya_s_glare.html?'+map_url_params);
	}

	app_loger('map generated id:' + rnd_file_name);


	// validate that the js on the page has been run and text is nor empty
	if (text_extra!="") {
		app_loger('waiting for text_extra :' + rnd_file_name);
		await page.waitForFunction(
		  'document.getElementById("div_text_extra").innerHTML!="";'
		);
		app_loger('verified for text_extra :' + rnd_file_name);
	}

	app_loger('generate_map - taking screenshot: id:' + rnd_file_name);
	await page.screenshot({path: 'generated_maps/'+rnd_file_name + '.png'});
	//app_loger('generate_map - creating a thumbnail id:' + rnd_file_name);
	//await sharp('generated_maps/'+rnd_file_name + '.png')
	//.resize(50,70)
	//.toFile('generated_maps/'+rnd_file_name + '_t.png');
	app_loger('generate_map - closing page: id:' + rnd_file_name);
	await page.close();
	var pages = await browser.pages();
	app_loger('number of open pages: ' + pages.length);


	app_loger('generate_map - replying:' + rnd_file_name);
	res.send('/generated_maps/' + rnd_file_name + '.png');
	app_loger('generate_map end' + " id:" + rnd_file_name);
});

app.get('/admin/generate_order_map', async (req,res) => {
	console.time('generate_order_map');
	app_loger('generate_order_map begin');

    var loc = req.query.loc;
	var loc_lng = req.query.loc_lng;
	var loc_lat = req.query.loc_lat;
	var date = req.query.date;
	var text_extra = req.query.text_extra;
	var bg_color = req.query.bg_color;


	var is_glare = req.query.is_glare;
	var is_constellations = req.query.is_constellations;
	var is_white_bg = req.query.is_white_bg;
	var is_grid = req.query.is_grid;
  var size = req.query.size;

	var map_url_params="";
	map_url_params += "loc="+loc;
	map_url_params += "&loc_lng="+loc_lng;
	map_url_params += "&loc_lat="+loc_lat;
	map_url_params += "&text_extra="+encodeURIComponent(text_extra);
	map_url_params += "&bg_color="+bg_color;
	map_url_params += "&is_glare="+is_glare;
	map_url_params += "&is_constellations="+is_constellations;
	map_url_params += "&is_grid="+is_grid;
	map_url_params += "&is_white_bg="+is_white_bg;
	map_url_params += "&date="+date;

  var y_hight=5000;
  if (size=='20x30' || size == '40x60') {
    map_url_params += "&content_y=5400";
    y_hight=5400;
  }
  else if (size=='30x40') {
    map_url_params += "&content_y=4800";
    y_hight=4800;
  }
  else if (size=='50x70') {
    map_url_params += "&content_y=5040";
    y_hight=5040;
  }
  var rnd_file_name = uuidv1();
  rnd_file_name = "order_"+rnd_file_name;

	var page = await browser.newPage();
	await page.setViewport({ width: 3600, height: y_hight});

if (is_glare=='false') {
		app_loger('generate_order_map no glare query:' + map_url_params);
		await page.goto('http://0.0.0.0:'+http_port+'/secret/iliya.html?'+map_url_params, {timeout: 5000, waitUntil: ['domcontentloaded']});
	}
	else {
		app_loger('generate_order_map with glare query:' + map_url_params);

		if (bg_color=="010203" || bg_color=="1a1f2a") {
			map_url_params = "glare_color=blue&"+map_url_params; // set star glare to blue to if black map (important to keep the date at the end)
		}
		else {
			map_url_params = "glare_color=white&"+map_url_params; // set star glare to white if not black map (important to keep the date at the end)
		}

		await page.goto('http://0.0.0.0:'+http_port+'/secret/iliya_glare.html?'+map_url_params, {timeout: 5000, waitUntil: ['domcontentloaded']});
	}

	// validate that the js on the page has been run and text is nor empty
	if (text_extra!="") {
		await page.waitForFunction(
		  'document.getElementById("div_text_extra").innerHTML!="";'
		);
	}

	await page.screenshot({path:'generated_maps/'+rnd_file_name + '.png'});

	app_loger('generate_order_map - closing page: id:' + rnd_file_name);
	await page.close();
	var pages = await browser.pages();
	app_loger('number of open pages: ' + pages.length);

	res.send('<a style="font-size:16px" href="/' + 'generated_maps/' + rnd_file_name + '.png' + '">generated_maps/'+rnd_file_name +'.png</a>');
	console.timeEnd('generate_order_map');
	app_loger('generate_order_map end');
});

app.use('/en', express.static('../tekamor_website/en/index.html'));
app.use('/en/privacy', express.static('../tekamor_website/en/privacy.html'));
app.use('/en/skymap_mobile', express.static('../tekamor_website/en/skymap_mobile.html'));
app.use('/en/checkout', express.static('../tekamor_website/en/checkout.html'));
app.use('/en/skymap', express.static('../tekamor_website/en/skymap.html'));
app.use('/en/thankyou', express.static('../tekamor_website/en/thankyou.html'));
app.use('/en/contact', express.static('../tekamor_website/en/contactus.html'));
app.use('/en/delivery', express.static('../tekamor_website/en/delivery.html'));
app.use('/en/faq', express.static('../tekamor_website/en/faq.html'));

app.use('/nightskyposter', express.static('../tekamor_website/nightskyposter.html'));
app.use('/privacy', express.static('../tekamor_website/privacy.html'));
app.use('/skymap_mobile', express.static('../tekamor_website/skymap_mobile.html'));
app.use('/checkout', express.static('../tekamor_website/checkout.html'));
app.use('/skymap', express.static('../tekamor_website/skymap.html'));
app.use('/thankyou', express.static('../tekamor_website/thankyou.html'));
app.use('/contact', express.static('../tekamor_website/contactus.html'));
app.use('/delivery', express.static('../tekamor_website/delivery.html'));
app.use('/faq', express.static('../tekamor_website/faq.html'));

app.use('/generated_maps', express.static('generated_maps'));
app.use('/generated_orders', express.static('generated_orders'));
app.use(express.static('../tekamor_website'));
//app.listen(port, () => console.log(`website running at port ${port}!`));

var httpServer = http.createServer(app);
httpServer.listen(http_port);


var httpsServer = https.createServer(credentials, app);
httpsServer.listen(https_port);



})();
