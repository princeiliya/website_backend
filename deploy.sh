#!/bin/sh
ssh_key_location="/Users/iliya.prince/git/LightsailDefaultKey-eu-central-1.pem"
backend_directory="/Users/iliya.prince/git/website_backend/"
frontend_directory="/Users/iliya.prince/git/tekamor_website/"
release_frontend_directory="/Users/iliya.prince/git/tekamor_website_release/"
deployment_dest_ip="3.120.232.30"

echo "Minifying html code"
html-minifier --collapse-whitespace --remove-comments --remove-optional-tags  --remove-script-type-attributes --remove-tag-whitespace --use-short-doctype --minify-css true --minify-js true --input-dir $frontend_directory --output-dir $release_frontend_directory --file-ext "html"

echo "Deploying new version of website"
rsync -avL --progress -e "ssh -i $ssh_key_location" $frontend_directory/* bitnami@$deployment_dest_ip:~/www/tekamor_website/
rsync -avL --progress -e "ssh -i $ssh_key_location" $release_frontend_directory/* bitnami@$deployment_dest_ip:~/www/tekamor_website/
rsync -avL --progress -e "ssh -i $ssh_key_location" $backend_directory/webserver.js bitnami@$deployment_dest_ip:~/www/website_backend/webserver.js
rsync -avL --progress -e "ssh -i $ssh_key_location" $backend_directory/order_confirmation_template.html bitnami@$deployment_dest_ip:~/www/website_backend/order_confirmation_template.html
rsync -avL --progress -e "ssh -i $ssh_key_location" $backend_directory/order_confirmation_item_template.html bitnami@$deployment_dest_ip:~/www/website_backend/order_confirmation_item_template.html


echo "Copied to server, restarting the backend"
RESTART_SERVER_SCRIPT="cd www/website_backend/;sudo fuser -k 80/tcp;nohup sudo node webserver.js > /dev/null 2> /dev/null < /dev/null &"
ssh -i $ssh_key_location -o StrictHostKeyChecking=no bitnami@$deployment_dest_ip "${RESTART_SERVER_SCRIPT}"
echo "server retarted"

echo "deleteing maps images older then 30 days"
CLEANUP_MAPS_SCRIPT="find www/website_backend/generated_maps -name \"*.png\" -type f -mtime +30 -exec rm -f {} \;"
ssh -i $ssh_key_location -o StrictHostKeyChecking=no bitnami@$deployment_dest_ip "${CLEANUP_MAPS_SCRIPT}"
echo "images deleted"
